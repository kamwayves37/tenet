<?php

namespace Demo;

use Database\SubscriberRepository;
use Room\Lifecycle\ViewModelProviderFactory;

class SubscriberViewModelFactory extends ViewModelProviderFactory
{
    /**
     * @var SubscriberRepository
     */
    private $repository;

    public function __construct(SubscriberRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create($modelClass)
    {
        if (is_subclass_of($modelClass, SubscriberViewModel::class)) {
            return new SubscriberViewModel($this->repository);
        }

        throw new \InvalidArgumentException("Unknown View Model Class");
    }
}
