<?php

namespace Demo;

use Database\Subscriber;
use Database\SubscriberRepository;
use Room\DataBinding\Observable;
use Room\Lifecycle\LiveData;
use Room\Lifecycle\ViewModel;
use Room\Observable\OnPropertyChangedCallback;

class SubscriberViewModel extends ViewModel implements Observable
{
    /**
     * @var SubscriberRepository
     */
    private $repository;

    public function __construct(SubscriberRepository $repository)
    {
        $this->repository = $repository;
    }

    public function saveOrUpdate(array $data)
    {
        $name = isset($data['name']) ? $data['name'] : "";
        $email = isset($data['email']) ? $data['email'] : "";
        $this->insert(new Subscriber(0, $name, $email));
    }

    public function clearAllOrDelete()
    {
        $this->clearAll();
    }

    /**
     * @return LiveData 
     */
    public function getSubscribers(): LiveData
    {
        return $this->repository->getAllSubscribers();
    }

    public function insert(Subscriber $subscriber)
    {
        $this->launch(function () use ($subscriber) {
            $this->repository->insert($subscriber);
        });
    }

    public function update(Subscriber $subscriber)
    {
        $this->launch(function () use ($subscriber) {
            $this->repository->update($subscriber);
        });
    }

    public function delete(Subscriber $subscriber)
    {
        $this->launch(function () use ($subscriber) {
            $this->repository->delete($subscriber);
        });
    }

    public function clearAll()
    {
        $this->launch(function () {
            $this->repository->deleteAll();
        });
    }

    public function addOnPropertyChangedCallback(OnPropertyChangedCallback $callable)
    {
    }

    public function removeOnPropertyChangedCallback(OnPropertyChangedCallback $callable)
    {
    }
}
