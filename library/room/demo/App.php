<?php

namespace Demo;

use Database\SubscriberDatabase;
use Database\SubscriberRepository;
use Room\Lifecycle\ViewModelProvider;
use Room\Loader\ClassLoader;

class App
{
    private SubscriberViewModel $subscribeViewModel;

    public function __construct()
    {
        $dao = SubscriberDatabase::getInstance()->subscriberDao();
        $repository = new SubscriberRepository($dao);
        $factory = new SubscriberViewModelFactory($repository);
        $this->subscribeViewModel = (new ViewModelProvider($factory))->get(SubscriberViewModel::class);
    }

    public function displaySubscribeList()
    {
        $this->subscribeViewModel->getSubscribers()->observe(function ($subscribes) {
            var_dump($subscribes);
        });
    }
}
