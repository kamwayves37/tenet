<?php

namespace Database;

use Room\Room;
use Room\RoomDatabase;
use Room\Components\DataBase;

/**
 * @Database(
 *      entities = [Subscriber::class],
 *      version = 1
 * )
 * 
 * @author
 */
abstract class SubscriberDatabase extends RoomDatabase
{
    /**
     * @var null|SubscriberDatabase
     */
    private static ?SubscriberDatabase $INSTANCE = null;

    /**
     * @return SubscriberDao 
     */
    public abstract function subscriberDao(): SubscriberDao;

    /**
     * @return SubscriberDatabase 
     */
    public static function getInstance(): SubscriberDatabase
    {
        if (self::$INSTANCE === null) {
            self::$INSTANCE = Room::databaseBuilder(
                self::class,
                "subscriber_data_database"
            )->build();
        }
        return self::$INSTANCE;
    }

    /**
     * @return void 
     */
    public static function destroyInstance()
    {
        self::$INSTANCE->dispose();
        self::$INSTANCE = null;
    }
}
