<?php

namespace Database;

use Room\Lifecycle\LiveData;

class SubscriberRepository
{
    /**
     * @var SubscriberDao
     */
    private $dao;

    /**
     * @var LiveData
     */
    private $subscribers;

    final public function __construct(SubscriberDao $dao)
    {
        $this->dao = $dao;
        $this->subscribers = $dao->getAllSubscribers();
    }

    /**
     * @return Subscriber[] 
     */
    public function getAllSubscribers(): LiveData
    {
        return $this->subscribers;
    }

    public function insert(Subscriber $subscriber)
    {
        $this->dao->insertSubscribe($subscriber);
    }

    public function update(Subscriber $subscriber)
    {
        $this->dao->updateSubscribe($subscriber);
    }

    public function delete(Subscriber $subscriber)
    {
        $this->dao->deleteSubscribe($subscriber);
    }

    public function deleteAll()
    {
        $this->dao->deleteAll();
    }

    /**
     * @param string $name 
     * @return Subscriber[] 
     */
    public function geByName(string $name): LiveData
    {
        return $this->dao->getSubscriberByName($name);
    }
}
