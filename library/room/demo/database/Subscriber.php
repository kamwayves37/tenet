<?php

namespace Database;

/**
 * @Entity(tableName = 'subscriber_data_table')
 */
class Subscriber
{
    /**
     * @PrimaryKey(autoGenerate = true)
     * @ColumnInfo(name = "subscriber_id")
     * @var int
     */
    public int $id;

    /**
     * @ColumnInfo(name = "subscriber_name")
     * @var string
     */
    public string $name;

    /**
     * @ColumnInfo(name = "subscriber_email")
     * @var string
     */
    public string $email;
}
