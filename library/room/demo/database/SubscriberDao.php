<?php

namespace Database;

use Room\Lifecycle\LiveData;

/**
 * @Dao
 */
interface SubscriberDao
{
    /**
     * @Insert
     */
    function insertSubscribe(Subscriber $subscriber);

    /**
     * @Update
     */
    function updateSubscribe(Subscriber $subscriber);

    /**
     * @Delete
     */
    function deleteSubscribe(Subscriber $subscriber);

    /**
     * @Query("DELETE FROM subscriber_data_table")
     */
    function deleteAll();

    /**
     * @Query("SELECT * FROM subscriber_data_table WHERE name == :name")
     * @return Subscriber[]
     */
    function getSubscriberByName(string $name): LiveData;

    /**
     * @Query("SELECT * FROM subscriber_data_table")
     * @return Subscriber[]
     */
    function getAllSubscribers(): LiveData;
}
