<?php

namespace Annotation;

use Room\Annotation\Annotation;
use Room\Annotation\DefaultValue;
use Room\Annotation\Target;

/**
 * @Annotation
 * @Target(
 *  {
 *      'METHOD',
 *      'CLASS',
 *      'PROPERTY'
 *  }
 * )
 */
interface AnnotationTest
{
    /**
     * @DefaultValue(0)
     * @Required
     */
    public function x(): int;

    public function y();
}
