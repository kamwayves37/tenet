<?php

use Annotation\AnnotationTest;
use Room\Reflector\RoomAnnotationParser;

/**
 * @AnnotationTest(y = 2)
 */
class ClassTest
{
    /**
     * @AnnotationTest(x=299)
     * @var mixed
     */
    private $name;

    /**
     * @AnnotationTest(x=5, y=11)
     */
    public function myMethodA()
    {
    }

    /**
     * @AnnotationTest(y=11)
     */
    public function myMethodB()
    {
    }
}

context(AnnotationTest::class, function () {
    beforeAll(function () {
        $this->parser = new RoomAnnotationParser;
        $this->reflection = new \ReflectionClass(ClassTest::class);
    });
    test("::getAnnotationOfClass()", function () {
        $test = $this->parser->getAnnotationOfClass($this->reflection, AnnotationTest::class);
        expect($test->x())->toBe(0);
        expect($test->y())->toBe(2);
    });
    test("::getAnnotationOfMethod()", function () {
        $test = $this->parser->getAnnotationOfMethod($this->reflection, 'myMethodA', AnnotationTest::class);
        expect($test->x())->toBe(5);
        expect($test->y())->toBe(11);
    });
    test("::getAnnotationOfProperty", function () {
        $test = $this->parser->getAnnotationOfProperty($this->reflection, 'name', AnnotationTest::class);
        expect($test->x())->toBe(299);
        expect($test->y())->toBe(null);
    });
});
