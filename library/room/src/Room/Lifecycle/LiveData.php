<?php

namespace Room\Lifecycle;

class LiveData
{
    private $observer;

    /**
     * @param Closure $callable 
     * @return void 
     */
    public function observe($callable)
    {
        $this->observer = $callable;
    }
}
