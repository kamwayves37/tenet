<?php

namespace Room\Lifecycle;

class ViewModelProvider
{
    /**
     * @var ViewModelProviderFactory
     */
    private $factory;

    public function __construct($factory)
    {
        $this->factory = $factory;
    }

    public function get($class_name)
    {
        return $this->factory->create($class_name);
    }
}
