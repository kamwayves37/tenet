<?php

namespace Room\Lifecycle;

abstract class ViewModelProviderFactory
{
    public abstract function create($modelClass);
}
