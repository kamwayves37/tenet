<?php

namespace Room\Lifecycle;

abstract class ViewModel
{
    /**
     * @param callable $closure 
     * @return void 
     */
    protected function launch($closure)
    {
    }
}
