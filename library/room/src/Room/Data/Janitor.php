<?php

namespace Room\Data;

use Room\Analyser\EasyReflector;
use Room\Annotation;
use Room\Components\DataBase;
use Room\Exception\JanitorException;
use Room\Room;
use Room\RoomDatabase;

class Janitor
{
    /**
     * @var array
     */
    private $registers;

    /**
     * @var string
     */
    private $data_name;

    public function __construct()
    {
    }

    public function doYouKnow($data_name)
    {
        $locator = Room::container()
            ->get('config.locator')
            ->readContentLocator('janitor.register');

        $data = json_decode($locator, true);

        if (json_last_error() === JSON_ERROR_NONE) {
            if (isset($data[$data_name])) {
                $this->data_name = $data_name;
                $this->registers = $data[$data_name];
                return true;
            } else {
                return false;
            }
        } else {
            throw new JanitorException("Corrupted or damaged registry file.");
        }
    }

    public function getToKnow($class_database, $data_name)
    {
        $reflection = new \ReflectionClass($class_database);

        if ($reflection->getParentClass()->getName() === RoomDatabase::class) {
            if ($reflection->isAbstract()) {
                // $db = Annotation::ofClass($reflection, DataBase::class);
            }
        }


        die(PHP_EOL . "Die in " . Janitor::class);
    }
}
