<?php

namespace Room;

use Room\Builder\DatabaseBuilder;
use Room\Container\Box;

final class Room
{
    /**
     * @var Box
     */
    private static $box = null;

    static function container()
    {
        if (self::$box == null)
            self::$box = new Box;
        return self::$box;
    }

    /**
     * @param string $room_data_name 
     * @param string $db_name 
     * @return DatabaseBuilder 
     */
    static function databaseBuilder($class_database, $db_name)
    {
        $janitor = self::container()->get('data.janitor');

        if ($janitor->doYouKnow($db_name)) {
        } else {
            $janitor->getToKnow($class_database, $db_name);
        }

        return new DatabaseBuilder($janitor);
    }
}
