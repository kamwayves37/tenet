<?php

namespace Room\Analyser;

use Database\SubscriberDatabase;
use ReflectionClass;

class EasyReflector
{
    public static function reflect()
    {
    }

    public static function resolve($class, $arguments = [])
    {
    }

    public static function analyse($class_name)
    {
    }
}
