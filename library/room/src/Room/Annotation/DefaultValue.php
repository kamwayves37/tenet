<?php

namespace Room\Annotation;

use Room\Annotation\Target;

/**
 * @Annotation
 * @Target({'PROPERTY', 'METHOD'})
 */
interface DefaultValue
{
    /**
     * @Required
     */
    public function value();
}
