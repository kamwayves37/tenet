<?php

namespace Room\Annotation;

/**
 * @Room\Annotation\Annotation
 * @Room\Annotation\Target({'CLASS'})
 */
interface Annotation
{
}
