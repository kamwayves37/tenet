<?php

namespace Room\Annotation;

/**
 * @Annotation
 * @Target({'CLASS', 'METHOD', 'PROPERTY'})
 */
interface AnnotationDrap
{
    function value();
}
