<?php

namespace Room\Annotation;

/**
 * @Room\Annotation\Annotation
 * @Room\Annotation\Target({'CLASS'})
 */
interface Target
{
    const ANNOTATION_TARGET_ALL = 'ALL';
    const ANNOTATION_TARGET_CLASS = 'CLASS';
    const ANNOTATION_TARGET_METHOD = 'METHOD';
    const ANNOTATION_TARGET_PROPERTY = 'PROPERTY';

    /**
     * @Required
     * @return array
     */
    function allowedTargets();
}
