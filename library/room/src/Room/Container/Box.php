<?php

namespace Room\Container;

use Room\Config\FileLocator;
use Room\Data\Janitor;
use Room\Exception\BoxException;

class Box
{
    /**
     * @var array
     */
    private $container = [];


    public function __construct()
    {
        $this->set(
            'data.janitor',
            function () {
                return new Janitor;
            },
            true
        );

        $this->set(
            'config.locator',
            function () {
                return new FileLocator(
                    [
                        'dir' => getcwd() . DIRECTORY_SEPARATOR . "library/room/janitor",
                        'auto_create' => true,
                        'files' => [
                            'janitor.register' => 'register.json'
                        ]
                    ]
                );
            },
            true
        );

        $this->set(
            'template.locator',
            function () {
                return new FileLocator(
                    [
                        'dir' => getcwd() . DIRECTORY_SEPARATOR . "library/room/src/Room/Reflector/files",
                        'auto_create' => true,
                        'files' => [
                            'class.tpl' => 'class.tpl',
                            'method.tpl' => 'method.tpl',
                        ]
                    ]
                );
            },
            true
        );
    }

    /**
     * @param string $key 
     * @return mixed 
     */
    public function get($key)
    {
        if ($this->has($key)) {
            $entry = $this->container[$key]['entry'];
            if (is_callable($entry))
                return call_user_func_array($entry, []);
            else
                return $entry;
        } else {
            throw new BoxException("key {$key} not frond in container box");
        }
    }

    /**
     * @param string $key 
     * @param mixed $entry 
     * @param bool $security 
     * @return void 
     */
    public function set($key, $entry, $security = false)
    {
        if (!$this->has($key)  || (!$this->getSecure($key))) {
            $this->container[$key] = compact('entry', 'security');
        } else {
            throw new BoxException("Key {$key} is secured");
        }
    }

    /**
     * @param string $key 
     * @return bool 
     */
    public function has($key)
    {
        return isset($this->container[$key]);
    }


    /**
     * @param string $key 
     * @return bool 
     */
    private function getSecure($key)
    {
        if ($this->has($key))
            return $this->container[$key]['secure'];
        else
            return false;
    }
}
