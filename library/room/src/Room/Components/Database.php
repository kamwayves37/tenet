<?php

namespace Room\Components;

use Room\Annotation\Annotation;
use Room\Annotation\Target;

/**
 * @Annotation
 * @Target('CLASS')
 */
final class DataBase
{
    /**
     * @var array
     */
    public $entities;

    /**
     * @return int 
     */
    public $version = 1;

    public function __construct()
    {
        var_dump(func_get_args());
    }
}
