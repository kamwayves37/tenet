<?php

namespace Room;

use PDO;

class RoomDatabase
{
    /**
     * @var PDO
     */
    private $conn;

    final protected function __construct($profiler)
    {
        try {
            $this->conn = new PDO(
                $profiler->getDsn(),
                $profiler->getUsername(),
                $profiler->getPassword
            );
        } catch (\PDOException  $e) {
            die($e->getMessage());
            print "Error !: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    final protected function dispose()
    {
        $this->conn = null;
    }

    final protected function __destruct()
    {
        $this->dispose();
    }
}
