<?php

namespace Room\DataBinding;

use Room\Observable\OnPropertyChangedCallback;

interface Observable
{
    function addOnPropertyChangedCallback(OnPropertyChangedCallback $callable);

    function removeOnPropertyChangedCallback(OnPropertyChangedCallback $callable);
}
