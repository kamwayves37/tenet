<?php

namespace Room;

class Annotation
{
    /** Component annotation */
    const ENTITY = "@Entity";
    const DATABASE = "@Database";
    const DAO = "@Dao";

    /** */
    const PRIMARY_KEY = "@PrimaryKey";
    const COLUMN_INFO = "@ColumnInfo";

    /** Dao annotation */
    const INSERT = "@Insert";
    const UPDATE = "@Update";
    const DELETE = "@Delete";
    const QUERY = "@Query";
}
