<?php

namespace Room\Entities;

abstract class DataEntity
{
    /**
     * @param mixed|null $data 
     */
    public function __construct($data = null)
    {
    }
}
