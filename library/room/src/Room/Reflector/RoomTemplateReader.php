<?php

namespace Room\Reflector;

use Room\Room;

class RoomTemplateReader
{
    /**
     * @param array $data 
     * @param string $templateName 
     * @return void 
     */
    public static function reader($data, $templateName)
    {
        $locator = Room::container()->get('template.locator');
        return preg_replace(
            array_map(
                function ($key) {
                    return "/{{\s*{$key}\s*}}/";
                },
                array_keys($data)
            ),
            $data,
            $locator->readContentLocator($templateName)
        );
    }
}
