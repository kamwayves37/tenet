<?php

namespace Room\Reflector;

use Room\Annotation\Annotation;

class RoomAnnotationFactory
{
    private $_cache = array();

    /**
     * @param \ReflectionClass $refectionClass 
     * @param Annotation|null $default 
     * @return Annotation 
     */
    public function get($key)
    {
        if ($this->hasAnnotation($key)) {
            return $this->_cache[md5($key)];
        } else {
            return null;
        }
    }

    public function set($key, $annotationInstance)
    {
        if (!$this->hasAnnotation($key)) {
            return $this->_cache[md5($key)] = $annotationInstance;
        };
    }

    /**
     * @param ReflectionClass $refectionClass 
     * @return bool 
     */
    public function hasAnnotation($key)
    {
        return isset($this->_cache[md5($key)]);
    }
}
