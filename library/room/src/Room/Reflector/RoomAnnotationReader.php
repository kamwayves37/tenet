<?php

namespace Room\Reflector;

use Room\Annotation\Annotation;
use Room\Exception\RoomAnnotationException;

class RoomAnnotationReader
{

    public function __construct()
    {
    }

    /**
     * @param ReflectionClass $reflectionClass 
     * @return bool 
     */
    public function isAnnotation($reflectionClass)
    {
        if ($reflectionClass->getDocComment()) {
            if ($reflectionClass->isInterface()) {
                return $this->isAnnotationDefined(
                    $reflectionClass,
                    Annotation::class
                );
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param \ReflectionClass $reflectionClass 
     * @param string $annotationClass 
     * @return bool 
     */
    public function isAnnotationDefined($reflectionClass, $annotationClass)
    {
        $sortName = basename($annotationClass);
        if (preg_match("/\B@[\\\\\w]*{$sortName}\b/", $reflectionClass->getDocComment())) {
            return true;
        }
        return false;
    }

    /**
     * @param string $docCom 
     * @param string|null $type 
     * @return array
     * @throws RoomAnnotationException 
     */
    public function getDefinition($docCom, $type = null)
    {
        if ($type) {
            if (preg_match_all(
                "/\s*\*\s*\B@[\\\\\w]*({$type}\b)\(\s*(.*?)\s*\)(?:\s|$)/s",
                $docCom,
                $matches
            )) {
                return array_combine($matches[1], $matches[2]);
            }
        } else {
            if (preg_match_all(
                '/\s*\*\s*\B@([\\\\\w]+)\(\s*(.*?)\s*\)(?:\s|$)/s',
                $docCom,
                $matches
            )) {
                return $matches[1];
                // return array_combine($matches[1], $matches[2]);
            }
        }
        throw new RoomAnnotationException("Annotation definition syntax error");
    }

    /**
     * @param string $fileName 
     * @param string $annotationShortName 
     * @return string|false 
     */
    public function getAnnotationNamespace($fileName, $annotationShortName)
    {
        if (file_exists($fileName)) {
            $content = file_get_contents($fileName);
            if (preg_match("/\buse\s+([\\\\\w]+\b{$annotationShortName})\s*;/", $content, $matches)) {
                return $matches[1];
            } elseif (preg_match("/\buse\s+([\\\\\w]+)\s+as\s+{$annotationShortName}\s*;/", $content, $matches)) {
                return $matches[1];
            }
        }
        return false;
    }

    /**
     * @param string $fileName 
     * @param string $annotationShortName 
     * @return string|false 
     */
    public function getAnnotationNamespaceAlias($fileName, $annotationShortName)
    {
        $content = file_get_contents($fileName);
        if (preg_match("/\buse\s+[\\\\\w]+\b{$annotationShortName}\s+as\s+(\w+)\s*;/", $content, $matches)) {
            return $matches[1];
        }
        return false;
    }

    /**
     * @param string $annotationDef 
     * @param \ReflectionClass $annotationReflectionClass 
     * @return mixed 
     */
    public function getAnnotationArguments($annotationDef, $annotationReflectionClass)
    {
        if (preg_match('/^[{].*[}]$|^[\[].*[\]]$|^[\'].*[\']$|^["].*[""]$|^[\d.]+$|^[\\\\\w]+::class$|^@.*$/', $annotationDef, $matches)) {
            $name = $annotationReflectionClass->getMethods()[0]->getName();
            return $this->argumentsFormatter(array_combine([$name], $matches));
        } elseif (preg_match_all('/([\w]+)=({.*?}|true|false|null|\'.*?\'|".*?"|[\d.]+|[\\\\\w]+::class|@[\\\\\w]+\(.*?\))(?:,|$)/i', $annotationDef, $matchers)) {
            return $this->argumentsFormatter(array_combine($matchers[1], $matchers[2]));
        } else {
            throw new \InvalidArgumentException("Annotation argument exception in test.php");
        }
    }

    /**
     * @param array $data 
     * @return array 
     */
    private function argumentsFormatter($data)
    {
        foreach ($data as $key => $value) {
            $data[$key] = $this->stringFormatter($value);
        }

        return $data;
    }

    /**
     * @param string $type 
     * @return string 
     */
    public function stringTypeFormatter($type)
    {
        switch ($type) {
            case 'int':
                return 'integer';
                break;
            case 'float':
                return 'double';
                break;
            case 'bool':
                return 'boolean';
                break;
            default:
                return $type;
                break;
        }
    }

    /**
     * @param string $string 
     * @return mixed 
     * @throws InvalidArgumentException 
     */
    private function stringFormatter($string)
    {
        if (preg_match("/^['\"](.*)['\"]$/", $string, $matches)) {
            return strval($matches[1]);
        } elseif (preg_match('/^\d+$/', $string, $matches)) {
            return intval($matches[0]);
        } elseif (preg_match('/^\d+.\d+$/', $string, $matches)) {
            return floatval($matches[0]);
        } elseif (preg_match('/^null$/i', $string, $matches)) {
            return null;
        } elseif (preg_match('/^true$/i', $string, $matches)) {
            return true;
        } elseif (preg_match('/^false$/i', $string, $matches)) {
            return false;
        } elseif (preg_match('/^\[(.*)\]$/', $string, $matches)) {
            try {
                $result = [];
                @eval("\$result = [{$matches[1]}];");
                return $result;
            } catch (\Throwable $th) {
                return $string;
            }
        } elseif (preg_match('/^{(.*)}$/', $string, $matches)) {

            //if (preg_match_all('/{(.*)(},|}$)/U', $matches[1], $array_matches, PREG_SET_ORDER, 0))
            $result = [];
            foreach (explode(',', $matches[1]) as $value) {
                if (preg_match('/^\'(\w+)\'[:=](.*)$|^"(\w+)"[:=](.*)$/', $value, $matches)) {
                    $result[$matches[1]] = $this->stringFormatter($matches[2]);
                } else {
                    $result[] = $this->stringFormatter($value);
                }
            }
            return $result;
        } elseif (preg_match('/^(.*)::class$/', $string, $matches)) {
            try {
                if (
                    \class_exists($matches[1]) ||
                    \interface_exists($matches[1] ||
                        \trait_exists($matches[1]))
                ) {
                    return $matches[1];
                }
            } catch (\Throwable $th) {
                throw new \InvalidArgumentException("Annotation argument error class `$string` does not exist");
            }
        } elseif (preg_match('/^@(.*)$/', $string, $matches)) {
            var_dump($matches);
            return $string;
        } else {
            return $string;
        }
    }

    /**
     * @param \ReflectionClass|\ReflectionMethod $reflection 
     * @return void 
     */
    public function getAnnotationFlags($reflection)
    {
        preg_match_all('/\B@([\w]+)(\s[\w $\|]+|\s?$)/m', $reflection->getDocComment(), $matches);

        $arguments = array_combine(
            array_map(function ($key) {
                return strtolower($key);
            }, $matches[1]),
            $matches[2]
        );

        if (!$arguments) {
            return false;
        }

        foreach ($arguments as $key => $value) {
            $format = trim($value);

            $arguments[$key] = empty($format) ? true : $format;
        }

        return $arguments;
    }
}
