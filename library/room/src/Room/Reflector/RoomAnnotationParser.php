<?php

namespace Room\Reflector;

use Room\Annotation\Annotation;
use Room\Annotation\DefaultValue;
use Room\Annotation\Target;
use Room\Exception\RoomAnnotationException;

class RoomAnnotationParser
{
    /**
     * @var string
     */
    private $class_template;

    /**
     * @var string
     */
    private $method_template;

    /**
     * @var string
     */
    private $annotation_namespace;

    /**
     * @var RoomAnnotationFactory
     */
    private $_annotation_factory;

    /**
     * @var RoomAnnotationCache
     */
    private $_annotation_caches;

    /**
     * @var RoomAnnotationReader
     */
    private $_annotation_reader;

    public function __construct()
    {
        $this->class_template = 'class.tpl';
        $this->method_template = 'method.tpl';
        $this->annotation_namespace = 'Room\\Reflector\\Annotation';
        $this->_annotation_factory = new RoomAnnotationFactory;
        $this->_annotation_caches = new RoomAnnotationCache;
        $this->_annotation_reader = new RoomAnnotationReader($this, $this->_annotation_caches);
    }

    /**
     * @param \ReflectionClass $reflectionClass 
     * @param string|null $annotationClass 
     * @return object|array 
     * @throws RoomAnnotationException 
     * @throws InvalidArgumentException 
     */
    public function getAnnotationOfClass($reflectionClass, $annotationClass = null)
    {
        if ($reflectionClass instanceof \ReflectionClass) {
            return $this->getAnnotationWith($reflectionClass, Target::ANNOTATION_TARGET_CLASS, $annotationClass);
        } else {
            throw new \InvalidArgumentException("The argument `reflectionClass` is not an instance of \ReflectionClass.");
        }
    }

    /**
     * @param \ReflectionClass $reflectionClass 
     * @param string $methodName 
     * @param string|null $annotationClass 
     * @return object|array 
     * @throws RoomAnnotationException 
     * @throws InvalidArgumentException 
     */
    public function getAnnotationOfMethod($reflectionClass, $methodName, $annotationClass = null)
    {
        if ($reflectionClass->hasMethod($methodName)) {
            $method = $reflectionClass->getMethod($methodName);
            return $this->getAnnotationWith($method, Target::ANNOTATION_TARGET_METHOD, $annotationClass);
        } else {
            throw new RoomAnnotationException("Method name `{$methodName}` does not exist in class `{$reflectionClass->getName()}`");
        }
    }

    /**
     * @param \ReflectionClass $reflectionClass 
     * @param string|null $annotationClass 
     * @return array 
     * @throws RoomAnnotationException 
     * @throws InvalidArgumentException 
     */
    public function getAnnotationOfMethods($reflectionClass, $annotationClass = null)
    {
        $annotations = array();
        foreach ($reflectionClass->getMethods() as $method) {
            $annotations[$method->getName()] = $this->getAnnotationWith($method, Target::ANNOTATION_TARGET_METHOD, $annotationClass);
        }
        return $annotations;
    }

    /**
     * @param ReflectionClass $reflectionClass 
     * @param mixed $propertyName 
     * @param mixed|null $annotationClass 
     * @return object|Annotation|void 
     * @throws RoomAnnotationException 
     * @throws InvalidArgumentException 
     */
    public function getAnnotationOfProperty($reflectionClass, $propertyName, $annotationClass = null)
    {
        if ($reflectionClass->hasProperty($propertyName)) {
            $property = $reflectionClass->getProperty($propertyName);
            return $this->getAnnotationWith($property, Target::ANNOTATION_TARGET_PROPERTY, $annotationClass);
        } else {
            throw new RoomAnnotationException("Property name `{$propertyName}` does not exist in class `{$reflectionClass->getName()}`");
        }
    }

    /**
     * @param \ReflectionClass $reflectionClass 
     * @param string|null $annotationClass 
     * @return array 
     * @throws RoomAnnotationException 
     * @throws InvalidArgumentException 
     */
    public function getAnnotationOfProperties($reflectionClass, $annotationClass = null)
    {
        $annotations = array();
        foreach ($reflectionClass->getProperties() as $property) {
            $annotations[$property->getName()] = $this->getAnnotationWith($property, Target::ANNOTATION_TARGET_PROPERTY, $annotationClass);
        }
        return $annotations;
    }

    /**
     * @param \ReflectionClass|\ReflectionMethod|\ReflectionProperty $reflection 
     * @param string $allowedTarget 
     * @param string|null $annotationClass 
     * @return object|array 
     * @throws RoomAnnotationException 
     * @throws InvalidArgumentException 
     */
    private function getAnnotationWith($reflection, $allowedTarget, $annotationClass = null)
    {
        if ($annotationClass && is_string($annotationClass)) {
            $annotationName = $this->getAnnotationName($reflection, $annotationClass);
            $annotationNamespace = $this->getAnnotationNamespace(
                $this->getAnnotationFileName($reflection),
                $annotationClass
            );

            if ($annotationNamespace && $annotationName) {
                $annoRefClass = new \ReflectionClass($annotationNamespace);
                if ($this->_annotation_reader->isAnnotation($annoRefClass)) {
                    if ($this->parseAnnotationTarget($annoRefClass, $allowedTarget)) {
                        return $this->getAnnotationOf($reflection, $annotationNamespace, $annotationName);
                    } else {
                        throw new RoomAnnotationException("Target not allowed for class.");
                    }
                } else {
                    throw new RoomAnnotationException("`{$annoRefClass->getName()}` is not an annotation.");
                }
            } else {
                throw new RoomAnnotationException("Annotation `{$annotationClass}` is not defined.");
            }
        } else {
            $annotations = array();
            $annotations_def = $this->_annotation_reader->getDefinition($reflection->getDocComment());
            foreach ($annotations_def as $anno) {
                if (!empty($anno)) {
                    $annotations[$anno] = $this->getAnnotationWith($reflection, $allowedTarget, $anno);
                }
            }
            return $annotations;
        }
    }

    /**
     * @param \ReflectionClass $reflectionClass 
     * @param string $annotationClass 
     * @return string|false;
     * @throws RoomAnnotationException 
     */
    private function getAnnotationName($reflectionClass, $annotationClass)
    {
        if (!isset($this->_annotation_caches[$annotationClass])) {
            $annotationName = basename($annotationClass);
            if (!$this->_annotation_reader->isAnnotationDefined($reflectionClass, $annotationName)) {
                $alias = $this->_annotation_reader->getAnnotationNamespaceAlias(
                    $this->getAnnotationFileName($reflectionClass),
                    $annotationName
                );
                if ($alias) {
                    $annotationName = $alias;
                } else {
                    return false;
                }
            }
            $this->_annotation_caches[$annotationClass] = $annotationName;
            return $annotationName;
        } else {
            return $this->_annotation_caches[$annotationClass];
        }
    }

    /**
     * @param string $fileName 
     * @param string $annotationClass 
     * @return string|false
     * @throws RoomAnnotationException 
     */
    private function getAnnotationNamespace($fileName, $annotationClass)
    {
        $key = $fileName . $annotationClass;
        if (!isset($this->_annotation_caches[$key])) {
            if (!interface_exists($annotationClass)) {
                $annotationNamespace = $this->_annotation_reader->getAnnotationNamespace($fileName, $annotationClass);
                if (!empty($annotationNamespace) && $annotationNamespace) {
                    return $this->_annotation_caches[$key] = $annotationNamespace;
                } else {
                    return false;
                }
            } else {
                return $this->_annotation_caches[$key] = $annotationClass;
            }
        } else {
            return $this->_annotation_caches[$key];
        }
    }

    /**
     * @param ReflectionClass|\ReflectionMethod|\ReflectionProperty $reflection 
     * @return string 
     */
    private function getAnnotationFileName($reflection)
    {
        if ($reflection instanceof \ReflectionProperty) {
            return $reflection->getDeclaringClass()->getFileName();
        } else {
            return $reflection->getFileName();
        }
    }

    /**
     * @param ReflectionClass $annotationReflectionClass 
     * @param string $annotationNamespace 
     * @param string $annotationName 
     * @return object 
     */
    private function getAnnotationOf($annotationReflectionClass, $annotationNamespace, $annotationName)
    {
        if ($this->_annotation_reader->isAnnotationDefined($annotationReflectionClass, $annotationName)) {
            $reflectionTargetClass = new \ReflectionClass($annotationNamespace);

            $annotationDef = \preg_replace(
                '/[*\s]+/',
                '',
                $this->_annotation_reader->getDefinition(
                    $annotationReflectionClass->getDocComment(),
                    $annotationName
                )[$annotationName]
            );

            $key = $annotationDef . $annotationNamespace . $annotationName;
            if (!$this->_annotation_factory->hasAnnotation($key)) {
                $args = $this->_annotation_reader->getAnnotationArguments($annotationDef, $reflectionTargetClass);
                if ($this->parseAnnotation($reflectionTargetClass, $args)) {
                    return $this->_annotation_factory
                        ->set(
                            $key,
                            $this->resolve($reflectionTargetClass, $args)
                        );
                }
            } else {
                return $this->_annotation_factory
                    ->get(
                        $key
                    );
            }
        } else {;
            throw new RoomAnnotationException("No annotation of type `{$annotationNamespace}` found in class name `{$annotationReflectionClass->getName()}`");
        }
    }

    /**
     * @param \ReflectionClass $annotationReflectionClass 
     * @param string $allowedTarget 
     * @return bool 
     * @throws RoomAnnotationException 
     * @throws InvalidArgumentException 
     */
    private function parseAnnotationTarget($annotationReflectionClass, $allowedTarget)
    {
        $annotationName = $this->getAnnotationName($annotationReflectionClass, Target::class);
        $annotationNamespace = $this->getAnnotationNamespace(
            $this->getAnnotationFileName($annotationReflectionClass),
            Target::class
        );
        if ($annotationName && $annotationNamespace) {
            if ($target = $this->getAnnotationOf($annotationReflectionClass, $annotationNamespace, $annotationName)) {
                if (in_array(Target::ANNOTATION_TARGET_ALL, $target->allowedTargets())) {
                    return true;
                }
                return in_array($allowedTarget, $target->allowedTargets());
            }
        }
        return true;
    }

    /**
     * @param ReflectionClass $reflectionClass 
     * @param mixed $Arguments 
     * @return bool 
     */
    private function parseAnnotation($reflectionClass, &$arguments)
    {
        foreach ($reflectionClass->getMethods() as $method) {
            $method_name = $method->getName();
            $flags = $this->_annotation_reader->getAnnotationFlags($method);
            $isRequired = isset($flags['required']) ? true : false;
            $arguments_method_exist = isset($arguments[$method_name]);


            if (!$arguments_method_exist) {
                $defaultValue = $this->getAnnotationDefaultValue($method);
                if ($defaultValue !== false) {
                    $arguments_method_exist = true;
                    $arguments[$method_name] = $defaultValue;
                }
            }

            if ($isRequired) {
                if (!$arguments_method_exist)
                    throw new RoomAnnotationException("Argument `{$method_name}` is required to annotation {$reflectionClass->getName()}");
            }

            if ($method->hasReturnType()) {
                $returnType = $this->_annotation_reader->stringTypeFormatter((string)$method->getReturnType());
                if ($returnType == 'mixed')
                    continue;
            } else {
                $returnType = isset($flags['return']) ? $flags['return'] : false;
                if ($returnType) {
                    $returnType = array_map(
                        function ($type) {
                            return $this->_annotation_reader->stringTypeFormatter($type);
                        },
                        explode('|', explode(' ', $returnType)[0])
                    );

                    if (in_array('mixed', $returnType))
                        continue;

                    if (count($returnType) == 1) {
                        $returnType = $returnType[0];
                    }
                }
            }

            if ($returnType && $arguments_method_exist) {
                if (is_array($returnType)) {
                    if (!in_array(gettype($arguments[$method_name]), $returnType)) {
                        throw new RoomAnnotationException("Compatibility error " . print_r($returnType, true) . "!= " . gettype($arguments[$method_name]));
                    }
                } else {
                    if ($returnType !== gettype($arguments[$method_name])) {
                        throw new RoomAnnotationException("Compatibility error {$returnType} != " . gettype($arguments[$method_name]));
                    }
                }
            }
        }

        return true;
    }

    /**
     * @param \ReflectionMethod|\ReflectionProperty $reflection 
     * @return mixed 
     * @throws RoomAnnotationException 
     * @throws InvalidArgumentException 
     */
    private function getAnnotationDefaultValue($reflection)
    {
        $annotationName = $this->getAnnotationName($reflection, DefaultValue::class);
        $annotationNamespace = $this->getAnnotationNamespace(
            $this->getAnnotationFileName($reflection),
            DefaultValue::class
        );
        if ($annotationName && $annotationNamespace) {
            if ($this->_annotation_reader->isAnnotationDefined($reflection, $annotationName)) {
                $default = $this->getAnnotationOf($reflection, $annotationNamespace, $annotationName);
                return $default->value();
            }
        }
        return false;
    }

    /**
     * @param ReflectionClass $reflectionClass 
     * @return Annotation 
     */
    private function resolve($reflectionClass, $data)
    {
        try {
            $annotation_name = "\\{$this->annotation_namespace}\\{$reflectionClass->getShortName()}";
            if (!class_exists($annotation_name)) {
                $classCode = $this->getAnnotationClassCode($reflectionClass);
                eval($classCode);
            }
            return new $annotation_name($data);
        } catch (\Throwable $th) {
            throw new RoomAnnotationException($th->getMessage());
        }
    }

    /**
     * @param ReflectionClass $reflectionClass 
     * @return string 
     */
    private function getAnnotationClassCode($reflectionClass)
    {
        return RoomTemplateReader::reader(
            [
                'namespace' => $this->annotation_namespace,
                'class_name' => $reflectionClass->getShortName(),
                'methods' => $this->getAnnotationPropertiesAndMethodCode($reflectionClass),
                'resolve_class_name' => $reflectionClass->getName()
            ],
            $this->class_template
        );
    }

    /**
     * @param ReflectionClass $reflectionClass 
     * @return string 
     */
    private function getAnnotationPropertiesAndMethodCode($reflectionClass)
    {
        $methods = [];
        $properties = [];
        foreach ($reflectionClass->getMethods() as $method) {
            $methods[] = RoomTemplateReader::reader(
                [
                    'method_name' => $method->getName(),
                    'return_type' => $method->hasReturnType() ? ':' . $method->getReturnType() : '',
                    'body' => "return \$this->_{$method->getName()};"
                ],
                $this->method_template
            );
            $properties[] = "private \$_{$method->getName()};";
        }
        return implode("\n", $properties) . PHP_EOL . implode("\n", $methods);
    }
}
