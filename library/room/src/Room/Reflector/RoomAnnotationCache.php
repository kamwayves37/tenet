<?php

namespace Room\Reflector;

class RoomAnnotationCache implements \ArrayAccess
{
    /**
     * @var array
     */
    private $container;

    public function __construct()
    {
        $this->container = array();
    }

    public function offsetExists($offset)
    {
        return isset($this->container[md5($offset)]);
    }

    public function offsetGet($offset)
    {
        $key = md5($offset);
        return isset($this->container[$key]) ? $this->container[$key] : null;
    }

    public function offsetSet($offset, $value)
    {
        if (!is_null($offset)) {
            $this->container[md5($offset)] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->container[md5($offset)]);
    }
}
