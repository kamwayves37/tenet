namespace {{namespace}}
{
    class {{class_name}} implements \Room\Annotation\Annotation, \{{resolve_class_name}}
    {
        public function __construct($data)
        {
            if(is_array($data)){
                foreach ($data as $key => $value) {
                    $property_name = '_' . $key;
                    if (property_exists($this, $property_name)) {
                        $this->$property_name = $value;
                    }else{
                        echo "Warning: Property $property_name is ignored because is not exist in " . __CLASS__ ."!\n";
                    }
                }
            }
        }

        {{methods}}
    }
}
