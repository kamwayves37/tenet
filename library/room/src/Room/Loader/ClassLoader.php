<?php

namespace Room\Loader;

use Room\Exception\ClassLoaderException;

class ClassLoader
{
    public static function interface_exists($namespace)
    {
        if (!\interface_exists($namespace, false)) {

            $class_file = self::_getFile($namespace);

            if (\file_exists($class_file)) {
                require_once $class_file;
                return \interface_exists($namespace, false);
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    private static function _getFile($namespace)
    {
        $base_dir =  dirname(__DIR__, 2);
        $full_name = $base_dir . DIRECTORY_SEPARATOR . $namespace;
        return str_replace('\\', '/', $full_name) . '.php';
    }

    public static function loader($namespace)
    {
        self::require(self::_getFile($namespace));
    }

    private static function require($class_file)
    {
        if (file_exists($class_file)) {
            require $class_file;
        } else
            throw new ClassLoaderException("The file '{$class_file}' cannot be found or does not exist");
    }

    /**
     * @param string $short_name 
     * @param string $source_name 
     * @return void 
     */
    public static function register()
    {
        spl_autoload_register(__NAMESPACE__ . '\ClassLoader::loader');
    }
}
