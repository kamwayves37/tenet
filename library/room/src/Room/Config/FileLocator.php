<?php

namespace Room\Config;

use InvalidArgumentException;
use Room\Exception\FileLocatorException;

class FileLocator
{
    /**
     * @var array
     */
    private $paths;

    /**
     * @var string
     */
    private $directory;

    /**
     * @param array $dir_config 
     * @throws InvalidArgumentException 
     * @throws FileLocatorException 
     */
    public function __construct($dir_config)
    {
        if (is_array($dir_config) && !empty($dir_config)) {
            if (isset($dir_config['dir'])) {
                $this->setDirectory($dir_config['dir']);
            }
            if (isset($dir_config['files'])) {
                $auto_create = (isset($dir_config['auto_create'])) ? $dir_config['auto_create'] : false;
                $files = $dir_config['files'];
                if (is_array($files)) {
                    foreach ($files as $key => $value) {
                        $this->register(
                            $key,
                            $value,
                            (is_bool($auto_create) ? $auto_create : false)
                        );
                    }
                } else {
                    throw new FileLocatorException("Illegal value of key ['files'] => ... in data config");
                }
            }
        } else {
            throw new InvalidArgumentException('Invalid argument type in the constructor');
        }
    }

    /**
     * @return string 
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * @param string $dir 
     * @return void 
     * @throws InvalidArgumentException 
     */
    public function setDirectory($dir)
    {
        $dir_path = realpath($dir);

        if (is_dir($dir_path)) {
            $this->directory = $dir_path;
        } else {
            throw new FileLocatorException("Invalid directory name {$dir} in data config");
        }
    }

    public function readContentLocator($filename)
    {
        $filepath = $this->locate($filename, true);
        $content = file_get_contents($filepath, FILE_USE_INCLUDE_PATH);
        if ($content !== false) {
            return $content;
        } else {
            throw new FileLocatorException("Error while reading the file {$filename}");
        }
    }

    /**
     * @param mixed $name 
     * @param bool $throwable 
     * @return mixed|null
     * @throws FileLocatorException 
     */
    public function locate($name, $throwable = false)
    {
        if (isset($this->paths[$name])) {
            return $this->paths[$name];
        } elseif ($throwable) {
            throw new FileLocatorException("{$name} does not exist in locator paths");
        }
    }

    /**
     * @param mixed $name 
     * @param mixed $file_path 
     * @param bool $auto_create 
     * @return void 
     * @throws FileLocatorException 
     */
    public function register($name, $filename, $auto_create = false)
    {
        $filename = ltrim($filename, "./");

        if (preg_match('/^\w+\S+\.\w{1,}$/', $filename)) {

            $absolute_name = $this->directory . DIRECTORY_SEPARATOR . $filename;

            if ($file_path = realpath($absolute_name)) {
                $this->paths[$name] = $file_path;
            } elseif ($auto_create) {
                $file_info = pathinfo($filename);

                $folder_directory = $this->directory . DIRECTORY_SEPARATOR . $file_info['dirname'];

                if (!is_dir($folder_directory)) {
                    if (!mkdir($folder_directory, 0777, true)) {
                        throw new FileLocatorException("Failed to create directories `{$folder_directory}`");
                    }
                }

                if ($file_info['extension'] === "json") {
                    $content = "{}";
                } else {
                    $content = "";
                }

                if (file_put_contents($absolute_name, $content) !== false) {
                    $file_path = realpath($absolute_name);
                    $this->paths[$name] = $file_path;
                } else {
                    throw new FileLocatorException("Error during the file creation process {$filename} in directory {$this->directory}");
                }
            } else {
                throw new FileLocatorException("{$filename} does not exist or is not a file in directory {$this->directory}");
            }
        } else {
            throw new FileLocatorException("`{$filename}` is not a correct filename");
        }
    }
}
