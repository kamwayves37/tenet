<?php


// $shoppingList = [
//     'diapers',
//     'kleenex',
//     'trash bags',
//     'paper towels',
//     'beer'
// ];

context(Matcher::class, function () {

    test("::toBeTrue", function () {
        expect(true)->toBeTrue();
        expect(false)->notToBeTrue();
        expect(1)->notToBeTrue();
    });

    test("::toBeFalse", function () {
        expect(false)->toBeFalse();
        expect(true)->notToBeFalse();
        expect(-1)->notToBeFalse();
    });

    test("::toBeTruthy", function () {
        expect(true)->toBeTruthy();
        expect(1)->toBeTruthy();
        expect('foo')->toBeTruthy();
        expect([])->toBeTruthy();
        expect(new stdClass)->toBeTruthy();
        expect(0)->notToBeTruthy();
        expect(-10)->notToBeTruthy();
        expect(null)->notToBeTruthy();
        expect(false)->notToBeTruthy();
    });

    test("::toBeFalsey", function () {
        expect(-10)->toBeFalsey();
        expect(null)->toBeFalsey();
        expect(false)->toBeFalsey();
        expect(true)->notToBeFalsey();
        expect(1)->notToBeFalsey();
        expect('foo')->notToBeFalsey();
        expect([])->notToBeFalsey();
        expect(new stdClass)->notToBeFalsey();
    });

    test("::toBeNull", function () {
        expect(null)->toBeNull();
    });

    test('::toBeEmpty', function () {
        expect([])->toBeEmpty();
        expect('')->toBeEmpty();
        expect(" ")->notToBeEmpty();
        expect([1])->notToBeEmpty();
    });

    test('::toBe', function () {
        $rand = rand();
        expect(1)->toBe(1);
        expect([1, 2, 9, $rand])->toBe([1, 2, 9, $rand]);
        expect(false)->toBe(false);
        expect("spec")->toBe("spec");
        expect(true)->notToBe(false);
        expect(0)->notToBe(false);
        expect(1)->notToBe(true);
        expect(1)->notToBe("1");
        expect(5)->notToBe(10);
        expect("")->notToBe(null);
        expect("Specum")->notToBe("specum");
        expect([1, 2, 9])->notToBe([$rand, 2]);
    });

    test('::toBeBetween', function () {
        expect(rand(-10, 10))->toBeBetween(-10, 10);
        expect(10)->toBeBetween(-10, 10);
        expect(-10)->toBeBetween(-10, 10);
        expect(19)->notToBeBetween(-10, 10);
        expect(-17)->notToBeBetween(-10, 10);
    });

    test('::toMatch', function () {
        expect('Condominium')->toMatch('/^Condo/');
        expect('foo')->notToMatch('/T/');
    });

    test('::toThrow', function () {
        $callable = function () {
            throw new Error('you are using the wrong callable');
        };

        expect($callable)->toThrow();
        expect($callable)->toThrow(Error::class);
        expect($callable)->toThrow('you are using the wrong callable');
        expect($callable)->toThrow('/callable$/');
        expect($callable)->notToThrow('/throw/');
        expect($callable)->notToThrow(Exception::class);
        expect(function () {
            return true;
        })->notToThrow();
    });
});
