<?php

use Specum\Expectation;

context(Expectation::class, function () {
    given('randValue', function () {
        return rand();
    });

    beforeEach(function () {
        $this->expectation = new Expectation($this->randValue);
    });

    test('::getActual', function () {
        expect($this->randValue)->toBe($this->expectation->getActual());
    });

    context('__call', function () {
        test('::notToBeNull', function () {
            //expect($this->randValue)->toBe($this->expectation->getActual());
        });
    });
});
