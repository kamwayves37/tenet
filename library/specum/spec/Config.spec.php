<?php

use Specum\Config;

context(Config::class, function () {
    given('patch', function () {
        return "./tst_specum.json";
    });

    beforeAll(function () {
        if (file_exists($this->patch)) {
            unlink($this->patch);
        }
    });

    beforeEach(function () {
        $this->config = new Config($this->patch);
        $this->config->reset();
    });

    test('::set', function () {
        $this->config->set('key_test', true);
        expect($this->config->get('key_test'))->toBeTrue();
    });

    test('::get', function () {
        expect($this->config->get('version'))->toBe('1.1');
    });

    test('::save', function () {
        $this->config->set('version', '2.2');
        expect($this->config->get('version'))->toBe('2.2');
        $this->config->save();
        $config_test = new Config($this->patch);
        expect($config_test->get('version'))->toBe('2.2');
    });

    test('::reset', function () {
        $this->config->set('version', '2.2');
        expect($this->config->get('version'))->toBe('2.2');
        $this->config->save();
        $this->config->reset();
        expect($this->config->get('version'))->toBe('1.1');
    });

    afterAll(function () {
        if (file_exists($this->patch)) {
            unlink($this->patch);
        }
    });
});
