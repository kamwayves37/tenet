<?php

use Specum\Matchers\Profiler;
use Specum\Process;

/**
 * @param string $msg 
 * @param callable $closure 
 * @return void 
 */
function context($msg, $closure)
{
    Process::suite()->context($msg, $closure);
}

/**
 * @param string $msg 
 * @param callable $closure 
 * @return void 
 */
function test($msg = "", $closure = null)
{
    Process::suite()->test($msg, $closure);
}

/**
 * @param callable $closure 
 * @return void 
 */
function beforeAll($closure)
{
    Process::suite()->beforeAll($closure);
}

/**
 * @param callable $closure 
 * @return void 
 */
function beforeEach($closure)
{
    Process::suite()->beforeEach($closure);
}

/**
 * @param callable $closure 
 * @return void 
 */
function afterAll($closure)
{
    Process::suite()->afterAll($closure);
}

/**
 * @param callable $closure 
 * @return void 
 */
function afterEach($closure)
{
    Process::suite()->afterEach($closure);
}

/**
 * @param string $name 
 * @param callable $closure 
 * @return void 
 */
function given($name, $closure)
{
    Process::suite()->given($name, $closure);
}

/**
 * @param mixed $entry 
 * @return Profiler 
 */
function expect($actual)
{
    return Process::suite()->expect($actual);
}
