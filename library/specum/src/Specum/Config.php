<?php

namespace Specum;

use Specum\Exception\ConfigException;

class Config
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * @var array
     */
    private $default;

    /**
     * @var string
     */
    private $patch_json;

    public function __construct($patch_json)
    {
        $this->default = [
            'name' => SPECUM,
            'version' => "1.1",
            'working_directory' => dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'spec' . DIRECTORY_SEPARATOR,
            'namespace_register' => [
                'Specum' => dirname(__DIR__, 2) . DS . 'src' . DS
            ]
        ];

        if (!is_dir($patch_json)) {
            if (pathinfo($patch_json)['extension'] === 'json') {
                $this->patch_json = $patch_json;
                if (file_exists($patch_json)) {
                    $file = file_get_contents($patch_json, FILE_USE_INCLUDE_PATH);
                    $content = json_decode($file, true);
                    if (json_last_error() === JSON_ERROR_NONE) {
                        $this->data = $content;
                    } else {
                        throw new ConfigException("message...");
                    }
                } else {
                    $content = json_encode($this->default);
                    $fp = fopen($patch_json, 'wb');
                    fwrite($fp, $content);
                    fclose($fp);
                }
            } else {
                throw new ConfigException("message...");
            }
        } else {
            throw new ConfigException("message...");
        }
    }

    /**
     * @param mixed $key 
     * @param mixed $value 
     * @return void 
     */
    public function set($key, $value)
    {
        $this->data[$key] = $value;
    }

    /**
     * @param mixed $key 
     * @return mixed 
     */
    public function get($key)
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        } elseif (isset($this->default[$key])) {
            return $this->default[$key];
        } else {
            throw new ConfigException("msg....");
        }
    }

    /**
     * @return void 
     */
    public function save()
    {
        file_put_contents($this->patch_json, json_encode($this->data), LOCK_EX);
    }

    /**
     * @return void 
     */
    public function reset()
    {
        $this->data = [];
        file_put_contents($this->patch_json, '{}', LOCK_EX);
    }
}
