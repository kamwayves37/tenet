<?php

namespace Specum\Cli;

use Specum\Container\Container;
use Specum\Exception\BoxException;
use Specum\Exception\ConsoleException;
use Specum\Exception\ContainerException;

class Console
{
    /**
     * @var array
     */
    private $long_opts;

    /**
     * @var array
     */
    private $arguments = [];

    /**
     * @param array $config 
     * @return void 
     */
    public function __construct()
    {
        $this->long_opts = [
            'src:'
        ];
    }

    /**
     * @return array 
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @param mixed $argv 
     * @return void 
     */
    public function setOpt($argv)
    {
        $optind = null;
        $this->set_option_config(getopt('', $this->long_opts, $optind));
        $this->filter_arguments(array_slice($argv, $optind));
    }

    /**
     * @param mixed $options 
     * @return void 
     * @throws ContainerException 
     * @throws BoxException 
     * @throws ConsoleException 
     */
    private function set_option_config($options)
    {
        if (!empty($options)) {
            foreach ($options as $key => $value) {
                $config = Container::get('box.specum')->get('config');
                switch ($key) {
                    case 'src':
                        if (is_dir($value)) {
                            $dir = realpath($value);
                            $config->set('working_directory', $dir);
                        } else {
                            throw new ConsoleException("`{$value}` cannot be set as working directory");
                        }
                        break;
                }
                $config->save();
            }
        }
    }

    /**
     * @param mixed $arguments 
     * @return void 
     */
    private function filter_arguments($arguments)
    {
        if (!empty($arguments)) {
            foreach ($arguments as $arg) {
                if (file_exists($arg)) {
                    $this->arguments[] = $arg;
                }
            }
        }
    }
}
