<?php

namespace Specum\Cli;

class Color
{
    /**
     * @var mixed
     */
    private $colors;

    function __construct()
    {
        $this->colors = [
            'foreground' => [
                'Default' => '39',
                'Black' => '30',
                'Red' => '31',
                'Green' => '32',
                'Yellow' => '33',
                'Blue' => '34',
                'Magenta' => '35',
                'Cyan' => '36',
                'Light gray' => '37',
                'Dark gray' => '90',
                'Light red' => '91',
                'Light green' => '92',
                'Light yellow' => '93',
                'Light blue' => '94',
                'Light magenta' => '95',
                'Light cyan' => '96',
                'White' => '97'
            ],
            'background' => [
                'Default' => '49',
                'Black' => '40',
                'Red' => '41',
                'Green' => '42',
                'Yellow' => '43',
                'Blue' => '44',
                'Magenta' => '45',
                'Cyan' => '46',
                'Light gray' => '47',
                'Dark gray' => '100',
                'Light red' => '101',
                'Light green' => '102',
                'Light yellow' => '103',
                'Light blue' => '104',
                'Light magenta' => '105',
                'Light cyan' => '106',
                'White' => '107'
            ]
        ];
    }

    /**
     * @param string $text 
     * @param string $fg_color_name 
     * @param string $bg_color_name 
     * @return string 
     */
    public function coloredText($text, $fg_color_name = 'Default', $bg_color_name = "")
    {
        $colored_text = "";

        if (isset($this->colors['foreground'][$fg_color_name])) {
            $colored_text .= "\e[" . $this->colors['foreground'][$fg_color_name] . "m";
        }

        if (isset($this->colors['background'][$bg_color_name])) {
            $colored_text .= "\e[" . $this->colors['background'][$bg_color_name] . "m";
        }

        return $colored_text . $text . "\e[0m";
    }
}
