<?php

namespace Specum\Cli;

use Specum\Container\Container;
use Specum\Util\Formatter;

class Journal extends Buffer
{
    /**
     * @var Color
     */
    private $color;

    public function __construct($color)
    {
        $this->color = $color;
    }

    /**
     * @param mixed $msg 
     * @return void 
     */
    public function header($msg)
    {
        $this->writeln($this->color
            ->coloredText(
                $msg,
                "Light yellow"
            ));
    }

    /**
     * @param Message $msg 
     * @return $this 
     */
    public function reporter($msg, $type, $spc)
    {
        switch ($type) {
            case true:
                $this->write($spc . $this->color
                    ->coloredText(
                        "✓  ",
                        "Light green"
                    ));
                $this->writeln($this->color
                    ->coloredText(
                        $msg,
                        "Dark gray"
                    ));
                break;
            case false:
                $this->writeln($spc . $this->color
                    ->coloredText(
                        "✗  " . $msg,
                        "Red"
                    ));
                break;
            default:
                $this->writeln($spc . $msg);
                break;
        }
    }

    /**@todo Review expectation debug object
     * @param mixed $object 
     * @param mixed $spc 
     */
    public function debugSpec($objects, $spc)
    {
        foreach ($objects as $obj) {
            $this->write(
                $this->color->coloredText(
                    "{$spc}     expectation {$obj['name']} failed in ",
                    'Red'
                )
            );
            $this->writeln(
                $this->color->coloredText(
                    "{$obj['file']}({$obj['line']})",
                    'Light blue'
                ),
                2
            );
            $this->write(
                $this->color->coloredText(
                    "{$spc}     █ ",
                    'Red'
                )
            );
            $this->writeln(
                $this->color->coloredText(
                    "actual:",
                    'Light red'
                )
            );
            $this->writeln("{$spc}          " . Formatter::normalize_var($obj['actual'], $spc . "          "), 2);
            $this->write(
                $this->color->coloredText(
                    "{$spc}     █ ",
                    'Green'
                )
            );
            $this->writeln(
                $this->color->coloredText(
                    ($obj['is_not'] ? 'not ' : '') . "expected:",
                    'Light green'
                )
            );

            $this->writeln("{$spc}          " . Formatter::normalize_var($obj['expected'], $spc . "          "), 2);
        }
    }

    /**
     * @return void 
     */
    public function publish()
    {
        echo $this->read();
    }
}
