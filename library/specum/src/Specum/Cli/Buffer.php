<?php

namespace Specum\Cli;

abstract class Buffer
{
    /**
     * @var string
     */
    private $string_cache = "";


    /**
     * @return string 
     */
    protected function read()
    {
        return $this->string_cache;
    }

    /**
     * @param string $msg 
     * @return $this 
     */
    public function write($msg)
    {
        $this->string_cache .= $msg;
        return $this;
    }

    /**
     * @param mixed $msg 
     * @param int $rln 
     * @return $this 
     */
    public function writeln($msg, $rln = 1)
    {
        $this->string_cache .=  $msg;
        $this->margin($rln);
        return $this;
    }

    /**
     * @param int $rln 
     * @return $this 
     */
    public function margin($rln = 1)
    {
        $this->string_cache .= str_repeat(PHP_EOL, $rln > 0 ? $rln : 1);
        return $this;
    }

    /**
     * @return $this 
     */
    protected function clean()
    {
        $this->string_cache = "";
        return $this;
    }
}
