<?php

namespace Specum;

class Scanner
{
    /**
     * @var string
     */
    private $directories;

    /**
     * @var array
     */
    private $files_patch = [];

    /**
     * @param string $directory 
     */
    public function __construct($files)
    {
        $this->directories = $files;

        foreach ($files as $path) {
            if (is_dir($path)) {
                $this->getDirContent($path, $this->files_patch);
            } elseif ($this->is_file_php($path)) {
                $this->files_patch[] = $path;
            }
        }
    }

    /**
     * @return string 
     */
    public function getDirs()
    {
        return $this->directories;
    }

    /**
     * @return array 
     */
    public function getSpecFiles()
    {
        return $this->files_patch;
    }

    /**
     * @param int $index 
     * @return string 
     */
    public function getSpecFile($index)
    {
        return $this->files_patch[$index];
    }


    /**
     * @param mixed $dir 
     * @return array 
     */
    private function getDirContent($dir, &$results = array())
    {
        $files = array_diff(scandir($dir), array('.', '..'));

        foreach ($files as $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (is_dir($path)) {
                $this->getDirContent($path, $results);
                if ($this->is_file_php($path)) {
                    $results[] = $path;
                }
            } else {
                if ($this->is_file_php($path)) {
                    $results[] = $path;
                }
            }
        }

        return $results;
    }

    private function is_file_php($file)
    {
        return preg_match('/\w+(.spec)(.php$)/i', basename($file)) === 1;
    }
}
