<?php

namespace Specum;

use Closure;
use Specum\Block\Test;
use Specum\Container\Box;
use Specum\Container\Container;
use Specum\Exception\ContainerException;
use Specum\Exception\BoxException;
use Specum\Exception\MatcherException;

final class Expectation
{
    /**
     * @var mixed
     */
    private $actual;

    /**
     * @var Closure[]
     */
    private $callable_matchers = [];

    /**
     * @param mixed $actual 
     * @param Test $parent 
     * @return void 
     */
    public function __construct($actual)
    {
        $this->actual = $actual;
    }

    /**
     * @return mixed 
     */
    public function getActual()
    {
        return $this->actual;
    }

    /**
     * @param mixed $name 
     * @param mixed $arguments 
     * @return $this|null 
     * @throws MatcherException 
     */
    function __call($name, $arguments)
    {
        $keywords = preg_split("/(?=[A-Z])/", $name);

        $isNot = false;

        if ($keywords[0] === 'not') {
            $isNot = true;
            array_shift($keywords);
        }

        $match_name = 'match.' . strtolower(implode('.', $keywords));

        if ($this->getMatchBox()->has($match_name)) {
            array_unshift($arguments, $this->actual);

            $matcher = $this->getMatchBox()->get($match_name);

            $trace = debug_backtrace(2)[0];

            $this->callable_matchers[$match_name] = function () use ($matcher, $arguments, $trace, $isNot) {
                $report = call_user_func_array($matcher['match'], $arguments);

                if ($isNot) {
                    $report['status'] = !$report['status'];
                }

                if (!$report['status']) {
                    $report['file'] = $trace['file'];
                    $report['line'] = $trace['line'];

                    $report['is_not'] = $isNot;
                    $report['description'] = sprintf($matcher['description'], $isNot ? ' not' : '');
                    $report['name'] = sprintf($matcher['name'], $isNot ? 'not ' : '');

                    return $report;
                }
                return false;
            };
        } else {
            throw new MatcherException("Match {$name} can't be found or can't be loaded correctly");
        }

        if ($isNot) {
            return $this;
        }
    }

    /**
     * @param string $name 
     * @return Box 
     * @throws ContainerException 
     * @throws BoxException 
     */
    private function getMatchBox()
    {
        return Container::get('box.matchers');
    }

    public function assess()
    {
        $reports = [];
        foreach ($this->callable_matchers as $match) {
            if ($report = $match()) {
                $reports[] = $report;
            }
        }
        return $reports;
    }
}
