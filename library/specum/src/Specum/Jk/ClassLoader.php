<?php

namespace Specum\JK;

use Specum\Container\Container;
use Specum\Exception\ClassLoaderException;

class ClassLoader
{
    public static function loader($namespace)
    {
        $root_name = explode('\\', $namespace)[0];
        if ($root_name === SPECUM) {
            $base_dir =  dirname(__DIR__, 2);
        } else {
            $config = Container::get('box.specum')->get('config');
            if ($root_register = $config->get('namespace_register')[$root_name]) {
                if ($base_dir = realpath($root_register)) {
                    if (is_file($base_dir)) {
                        throw new ClassLoaderException("Error Processing Request", 1);
                    }
                } else {
                    throw new ClassLoaderException("Error Processing Request", 1);
                }
            } else {
                throw new ClassLoaderException("Error Processing Request", 1);
            }
        }

        $full_name = $base_dir . DS . $namespace;
        $class_file =  str_replace('\\', '/', $full_name) . '.php';
        self::require($class_file);
    }

    private static function require($class_file)
    {
        if (file_exists($class_file)) {
            require $class_file;
        } else
            throw new ClassLoaderException("The file '{$class_file}' cannot be found or does not exist");
    }

    /**
     * @param string $short_name 
     * @param string $source_name 
     * @return void 
     */
    public static function register()
    {
        spl_autoload_register(__NAMESPACE__ . '\ClassLoader::loader');
    }
}
