<?php

namespace Specum;

use Loader\ClassMap;
use Loader\ClassMapLoader;
use Specum\Block\Block;
use Specum\Cli\Color;
use Specum\Cli\Console;
use Specum\Cli\Journal;
use Specum\Container\Box;
use Specum\Container\Container;
use Specum\Exception\ContainerException;
use Specum\Exception\BoxException;
use Specum\Runnable\Runnable;

class Specum
{
    /**
     * @return void 
     * @throws ContainerException 
     * @throws BoxException 
     */
    public static function save_state_and_exit()
    {
        exit(0);
    }

    /**
     * @return void 
     * @throws ContainerException 
     */
    public static function register_dependence()
    {
        //Container config
        Container::register("box.specum", function () {
            $box = new Box;

            //Setting
            $box->set(
                'config',
                new Config(realpath(getcwd()) . DIRECTORY_SEPARATOR . 'specum.json'),
                true
            );

            //Console
            $box->set(
                'cli.console',
                new Console(),
                true
            );

            //Color
            $box->set(
                'cli.color',
                new Color(),
                true
            );

            //Journal
            $box->set(
                'cli.journal',
                new Journal($box->get('cli.color')),
                true
            );

            //Group root
            $box->set('group.root', new Block(
                [
                    'name' => 'root'
                ]
            ), true);

            return $box;
        }, true);
    }

    /**
     * @param mixed $argv 
     * @return void 
     * @throws ContainerException 
     * @throws BoxException 
     */
    public static function set_cli_console_arg($argv)
    {
        Container::get('box.specum')
            ->get('cli.console')
            ->setOpt($argv);
    }

    /**
     * @return void 
     * @throws ContainerException 
     * @throws BoxException 
     */
    public static function  run_all_specs()
    {
        (new Runnable())->run();
    }

    /**
     * @param string $file_patch 
     * @return void 
     */
    public static function require($file_patch)
    {
        require_once $file_patch;
    }

    public static function loadClassMapping()
    {
        $mapping_file = getcwd() . DS . 'class.mapping.json';
        if (file_exists($mapping_file)) {
            $mapping = ClassMap::loadMap($mapping_file)['mappings'];
            if ($mapping) {
                $loader = new ClassMapLoader($mapping);
                $loader->register();
            }
        }
    }
}
