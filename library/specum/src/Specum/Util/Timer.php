<?php

namespace Specum\Util;

class Timer
{
    /**
     * @return float 
     */
    public static function now()
    {
        return round(microtime(1) * 1000, 2);
    }
}
