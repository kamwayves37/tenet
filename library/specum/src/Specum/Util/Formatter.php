<?php

namespace Specum\Util;

class Formatter
{
    public static function normalize_var($value, $dln = "")
    {
        switch (gettype($value)) {
            case 'array':
                $string = "";
                $string .= '(array) [' . PHP_EOL;
                $string .= implode(",\n", array_map(
                    function ($key, $val) use ($dln) {
                        return sprintf($dln . "  %s => %s", self::normalize_var($key), self::normalize_var($val, $dln . "       "));
                    },
                    array_keys($value),
                    $value
                ));
                $string .= PHP_EOL . $dln . ']';
                return $string;
                break;

            case 'integer':
            case 'double':
                return $value;
                break;

            case 'string':
                return '"' . $value . '"';
                break;

            case 'NULL':
                return "null";
                break;

            case 'object':
                return "object(" . get_class($value) . ")";
                break;

            case 'resource':
                return "resource(" . get_parent_class($value) . ")";
                break;

            case 'boolean':
                if ($value)
                    return "true";
                return "false";
                break;

            default:
                return "unknown";
                break;
        }
    }

    public static function print_string($entry, $dln = "", $return = false)
    {
        $string = self::normalize_var($entry, $dln);

        if ($return == true)
            return $string;

        echo $string . PHP_EOL;
    }
}
