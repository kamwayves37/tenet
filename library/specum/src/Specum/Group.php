<?php

namespace Specum;

use Closure;
use Specum\Block\Type;
use Specum\Cli\Journal;
use Specum\Container\Container;
use Specum\Exception\ContainerException;
use Specum\Exception\BoxException;
use Specum\Exception\GroupException;

abstract class Group
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var $this
     */
    protected $parent;

    /**
     * @var Closure
     */
    protected $closure;

    /**
     * @var Type
     */
    protected $type;

    /**
     * @var bool
     */
    protected $called = false;

    public function __construct($config = [])
    {
        $this->name = isset($config['name']) ? $config['name'] : "";
        $this->parent = isset($config['parent']) ? $config['parent'] : null;
        $this->closure = isset($config['closure']) ? $config['closure'] : function () {
        };
        $this->type = isset($config['type']) ? $config['type'] : null;
    }

    public function run($spc = SP)
    {
        $this->called = true;
        Process::setSuite($this);
    }

    /**
     * @return exit 
     * @throws GroupException 
     */
    protected function nextStep($newThis = null)
    {
        $this->call($this->closure, $newThis);
    }

    /**
     * @return Journal 
     * @throws ContainerException 
     * @throws BoxException 
     */
    public function getJournal()
    {
        return Container::get('box.specum')
            ->get('cli.journal');
    }

    /**
     * @param mixed $actual 
     * @throws GroupException 
     */
    public function expect($actual)
    {
        throw new GroupException("The specification is restricted in the {$this->name} block. It must be used in a test group");
    }

    /**
     * @return null 
     */
    protected function getObjBind()
    {
        return null;
    }

    /**
     * @param Closure $closure 
     * @param mixed|null $newThis 
     * @return mixed 
     * @throws GroupException 
     */
    protected function call($closure, $newThis = null)
    {
        try {
            if ($closure != null) {
                if (!is_object($newThis)) {
                    return call_user_func_array($closure, []);
                } else {
                    return $closure->call($newThis);
                }
            }
        } catch (\Exception $e) {
            throw new GroupException("Error during the test process. Check the syntax for reporting tests context.\n{$e->getMessage()}");
        }
    }

    /**
     * @return bool 
     */
    public abstract function getStatus();
}
