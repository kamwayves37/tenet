<?php

namespace Specum\Exception;

class ExpectationException extends \Exception
{
    private $report;

    public function __construct($report)
    {
        $this->report = $report;
    }

    public function getReport()
    {
        return $this->report;
    }
}
