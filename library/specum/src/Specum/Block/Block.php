<?php

namespace Specum\Block;

use Specum\Exception\GroupException;
use Specum\Group;
use Specum\Util\Scope;

class Block extends Group
{
    /**
     * @var \Closure[]
     */
    private $_callbacks = [];

    /**
     * @var Group[]
     */
    private $group_child = [];

    /**
     * @var object
     */
    private $global_scope;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->global_scope = new Scope;
    }

    function getChild()
    {
        return $this->group_child;
    }

    /**
     * @return object 
     */
    protected function getObjBind()
    {
        return $this->global_scope;
    }

    /**
     * @return bool 
     */
    public function getStatus()
    {
        foreach ($this->group_child as $group) {
            if (!$group->getStatus())
                return false;
        }
        return true;
    }

    /**
     * @param callable $closure 
     * @return void 
     */
    public function beforeAll($closure)
    {
        $this->_callbacks['before_all_test'] = $closure;
    }

    /**
     * @param callable $closure 
     * @return void 
     */
    public function afterAll($closure)
    {
        $this->_callbacks['after_all_test'] = $closure;
    }

    /**
     * @param callable $closure 
     * @return void 
     */
    public function beforeEach($closure)
    {
        $this->_callbacks['before_test'] = $closure;
    }

    /**
     * @param callable $closure 
     * @return void 
     */
    public function afterEach($closure)
    {
        $this->_callbacks['after_test'] = $closure;
    }

    /**
     * @param string $name 
     * @param callable $closure 
     * @return void 
     * @throws GroupException 
     */
    public function given($name, $closure)
    {
        $return = $this->call($closure);
        $this->call(function () use ($name, $return) {
            $this->$name = $return;
        }, $this->global_scope);
    }

    /**
     * @param string $name 
     * @param callable $closure 
     * @return void 
     */
    public function context($name, $closure)
    {
        $this->group_child[] = new self([
            'name' => $name,
            'closure' => $closure,
            'parent' => $this,
            'scope' => $this->global_scope,
            'type' => Type::CONTEXT
        ]);
    }

    /**
     * @param string $name 
     * @param callable $closure 
     * @return void 
     */
    public function test($name, $closure)
    {
        $this->group_child[] = new Test([
            'name' => $name,
            'closure' => $closure,
            'parent' => $this,
            'type' => Type::TEST
        ]);
    }

    private function sortGroupChild()
    {
        usort($this->group_child, function ($ga, $gb) {
            if ($ga->type === $gb->type)
                return 0;

            if ($ga->type !== Type::CONTEXT)
                return -1;
            else
                return 1;
        });
    }

    public function run($spc = "  ")
    {
        parent::run();

        $this->getJournal()->writeln($spc . $this->name);

        $this->nextStep();

        $this->sortGroupChild();

        $this->call($this->_callbacks['before_all_test'], $this->global_scope);

        foreach ($this->group_child as $group) {
            if (!$group->called) {
                if ($group->type === Type::TEST) {
                    $this->call($this->_callbacks['before_test'], $this->global_scope);
                    $group->run($spc . SP);
                    $this->call($this->_callbacks['after_test'], $this->global_scope);
                } else {
                    $this->getJournal()->margin();
                    $group->run($spc . SP);
                }
            }
        }

        $this->call($this->_callbacks['after_all_test'], $this->global_scope);
    }
}
