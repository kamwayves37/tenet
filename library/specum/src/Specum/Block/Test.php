<?php

namespace Specum\Block;

use Specum\Exception\GroupException;
use Specum\Exception\ContainerException;
use Specum\Exception\BoxException;
use Specum\Expectation;
use Specum\Group;
use Specum\Util\Timer;

class Test extends Group
{
    /**
     * @var mixed
     */
    protected $status = false;

    /**
     * @var Expectation[]
     */
    protected $specs = [];

    /**
     * @return bool 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $actual 
     * @return Expectation 
     */
    public function expect($actual)
    {
        return $this->specs[] = new Expectation($actual);
    }

    /**
     * 
     * @return array 
     */
    private function runSpec()
    {
        $reports = [];
        foreach ($this->specs as $spec) {
            if ($report = $spec->assess()) {
                $reports = array_merge($reports, $report);
            }
        }

        $this->status = empty($reports);

        return $reports;
    }

    /**
     * @param string $spc 
     * @return void 
     * @throws GroupException 
     * @throws ContainerException 
     * @throws BoxException 
     */
    public function run($spc = SP)
    {
        parent::run();

        $start_time = Timer::now();

        $this->nextStep($this->parent->getObjBind());

        $result = $this->runSpec();

        $timeout = round(Timer::now() - $start_time, 2);

        $this->getJournal()->reporter(
            $this->name . " ({$timeout}ms)",
            $this->status,
            $spc
        );

        if (!$this->status)
            $this->getJournal()->debugSpec($result, $spc);
    }
}
