<?php

namespace Specum\Block;

class Type
{
    const EXPECT = "expect";
    const TEST = "test";
    const CONTEXT = "context";
}
