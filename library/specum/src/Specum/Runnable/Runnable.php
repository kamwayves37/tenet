<?php

namespace Specum\Runnable;

use Specum\Container\Container;
use Specum\Scanner;
use Specum\Specum;

class Runnable
{
    /**
     * @var Scanner
     */
    private $scanner;

    /**
     * @var array
     */
    private $specs_files_name = [];

    /**
     * @var array
     */
    private $skip_specs_files_name = [];

    /**
     * @param Scanner $scanner 
     */
    public function __construct()
    {
        $args = Container::get('box.specum')
            ->get('cli.console')
            ->getArguments();

        if (empty($args)) {
            $args[] = Container::get('box.specum')
                ->get('config')
                ->get('working_directory');
        }

        $this->scanner = new Scanner($args);
    }

    /**
     * @return void 
     */
    private function require_spec($root)
    {
        foreach ($this->scanner->getSpecFiles() as $file_patch) {
            if (is_file($file_patch)) {
                $before_require_child_size = count($root->getChild());
                Specum::require($file_patch);
                $after_require_child_size = count($root->getChild());

                if ($after_require_child_size > $before_require_child_size) {
                    $this->specs_files_name[] = $file_patch;
                    $out_context = $after_require_child_size - $before_require_child_size;
                    for ($i = 1; $i < $out_context; $i++) {
                        $this->specs_files_name[] = "*";
                    }
                } else {
                    $this->skip_specs_files_name[] = $file_patch;
                }
            }
        }
    }

    /**
     * @param Group $group 
     * @return void 
     */
    public function run()
    {
        $root = Container::get('box.specum')
            ->get('group.root');

        $this->require_spec($root);

        $journal = Container::get('box.specum')
            ->get('cli.journal');

        foreach ($root->getChild() as $key => $group) {
            if ($this->specs_files_name[$key] != "*") {
                $journal->header("run spec in " . $this->specs_files_name[$key]);
            }
            $group->run();
            $journal->margin();
        }

        $journal->publish();

        //var_dump($root->getStatus());
    }
}
