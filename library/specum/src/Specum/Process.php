<?php

namespace Specum;

use Specum\Block\Block;
use Specum\Block\Test;
use Specum\Container\Container;

class Process
{
    /**
     * @var Group
     */
    private static $group = null;

    /**
     * @param mixed $suite 
     * @return void 
     */
    public static function setSuite($suite)
    {
        self::$group = $suite;
    }

    /**
     * @return Block|Test 
     */
    public static function suite()
    {
        if (self::$group === null)
            self::$group = Container::get('box.specum')->get('group.root');
        return self::$group;
    }
}
