<?php

namespace Specum\Container;

use Specum\Exception\BoxException;

class Box
{
    /**
     * @var array
     */
    private $container = [];

    /**
     * @param string $key 
     * @return mixed 
     */
    public function get($key)
    {
        if ($this->has($key)) {
            $entry = $this->container[$key]['entry'];
            if (is_callable($entry))
                return call_user_func_array($entry, []);
            else
                return $entry;
        } else {
            throw new BoxException("key {$key} not frond in container box");
        }
    }

    /**
     * @param string $key 
     * @param mixed $entry 
     * @param bool $security 
     * @return void 
     */
    public function set($key, $entry, $security = false)
    {
        if (!$this->has($key)  || (!$this->getSecure($key))) {
            $this->container[$key] = compact('entry', 'security');
        } else {
            throw new BoxException("Key {$key} is secured");
        }
    }

    /**
     * @param string $key 
     * @return bool 
     */
    public function has($key)
    {
        return isset($this->container[$key]);
    }


    /**
     * @param string $key 
     * @return bool 
     */
    private function getSecure($key)
    {
        if ($this->has($key))
            return $this->container[$key]['secure'];
        else
            return false;
    }
}
