<?php

namespace Specum\Container;

use Specum\Container\Box;
use Specum\Exception\ContainerException;

class Container
{
    /**
     * @var Box[]
     */
    private static $box = [];

    /**
     * @param string $box
     * @param bool $secure 
     * @return void 
     * @throws ContainerException 
     */
    public static function register($name, $box, $secure)
    {
        if (!self::has($name) || !self::secure($name)) {
            if (is_callable($box))
                $box = call_user_func_array($box, []);
            self::$box[$name] = compact('name', 'box', 'secure');
        } else
            throw new ContainerException("The box associated with the name '{$name}' already exists\nand cannot be redefined because it is defined in secure mode.");
    }

    /**
     * @param string $name 
     * @return Box 
     * @throws ContainerException 
     */
    public static function get($name)
    {
        if (self::has($name))
            return self::$box[$name]['box'];
        else
            throw new ContainerException("No Box corresponding to the name '{$name}'.");
    }

    /**
     * @param string $name 
     * @return bool 
     */
    public static function has($name)
    {
        return isset(self::$box[$name]);
    }

    /**
     * @param string $name 
     * @return bool 
     */
    public static function secure($name)
    {
        if (self::has($name))
            return self::$box[$name]['secure'];
        else
            return false;
    }
}
