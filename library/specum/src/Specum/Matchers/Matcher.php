<?php

namespace Specum\Matchers;

use Exception;
use Specum\Container\Box;
use Specum\Container\Container;

class Matcher
{
    public static function register()
    {
        $box = new Box;

        //To be true
        $box->set(
            'match.to.be.true',
            [
                'name' => "%sTo be true",
                'description' => "Check that the value is%s true",
                'match' => function ($actual) {
                    $expected = true;
                    $status = ($actual === $expected);
                    return compact('status', 'actual', 'expected');
                },
            ],
            true
        );

        //To be false
        $box->set(
            'match.to.be.false',
            [
                'name' => "%sTo be false",
                'description' => "Check that the value is%s false",
                'match' => function ($actual) {
                    $expected = false;
                    $status = ($actual === $expected);
                    return compact('status', 'actual', 'expected');
                },
            ],
            true
        );

        //To be truthy
        $box->set(
            'match.to.be.truthy',
            [
                'name' => "%sTo be truthy",
                'description' => "Check that the value is%s truthy",
                'match' => function ($actual) {
                    $expected = "!null|0+|true";
                    if (is_numeric($actual)) {
                        $status = $actual > 0;
                    } elseif (is_bool($actual)) {
                        $status = $actual === true;
                    } else {
                        $status = $actual !== null;
                    }

                    return compact('status', 'actual', 'expected');
                },
            ],
            true
        );

        //To be falsey
        $box->set(
            'match.to.be.falsey',
            [
                'name' => "%sTo be falsey",
                'description' => "Check that the value is%s falsey",
                'match' => function ($actual) {
                    $expected = "null|-0|false";
                    if (is_numeric($actual)) {
                        $status = $actual <= 0;
                    } elseif (is_bool($actual)) {
                        $status = $actual === false;
                    } else {
                        $status = $actual === null;
                    }

                    return compact('status', 'actual', 'expected');
                },
            ],
            true
        );

        //To be null
        $box->set(
            'match.to.be.null',
            [
                'name' => "%sTo be null",
                'description' => "Check that the value is%s null",
                'match' => function ($actual) {
                    $expected = null;
                    $status = ($actual === $expected);
                    return compact('status', 'actual', 'expected');
                },
            ],
            true
        );

        //To be empty
        $box->set(
            'match.to.be.empty',
            [
                'name' => "%sTo be empty",
                'description' => "Check that the value is%s empty",
                'match' => function ($actual) {
                    $expected = "void|[]|''";
                    $status = (empty($actual));
                    return compact('status', 'actual', 'expected');
                },
            ],
            true
        );

        //To be
        $box->set(
            'match.to.be',
            [
                'name' => "%sTo be",
                'description' => "Checks if a value is%s strictly equal",
                'match' => function ($actual, $expected) {
                    $status = ($actual === $expected);
                    return compact('status', 'actual', 'expected');
                },
            ],
            true
        );

        //To be between
        $box->set(
            'match.to.be.between',
            [
                'name' => "%sTo be between",
                'description' => "Checks if a value is%s between [min, max]",
                'match' => function ($actual, $min, $max) {

                    $expected = "between [$min, $max]";

                    $status = ($min <= $actual && $actual <= $max);

                    return compact('status', 'actual', 'expected');
                },
            ],
            true
        );

        //To match
        $box->set(
            'match.to.match',
            [
                'name' => "%sTo match",
                'description' => "Checks if a value is%s match",
                'match' => function ($actual, $pattern) {

                    $expected = $pattern;

                    $status = (preg_match($pattern, $actual) === 1);

                    return compact('status', 'actual', 'expected');
                },
            ],
            true
        );

        //To throw
        $box->set(
            'match.to.throw',
            [
                'name' => "%sTo throw",
                'description' => "Checks if a value is%s throw",
                'match' => function ($actual, $type = null) {

                    $expected = Exception::class;
                    $status = false;

                    if (is_callable($actual)) {
                        $throw = null;
                        try {
                            $actual();
                        } catch (\Throwable $th) {
                            $throw = $th;
                        }

                        if ($throw !== null) {
                            if ($type === null) {
                                $status = true;
                            } else {
                                $expected = $type;
                                if (is_object($type)) {
                                } elseif (is_string($type)) {
                                    if (class_exists($type)) {
                                        $status = $throw instanceof $type;
                                    } elseif (preg_match('/^\/\S+\/$/', $type) === 1) {
                                        $status = preg_match($type, $throw->getMessage()) === 1;
                                    } else {
                                        $status = $throw->getMessage() === $type;
                                    }
                                }
                            }
                        }
                    }

                    return compact('status', 'actual', 'expected');
                },
            ],
            true
        );

        Container::register("box.matchers", $box, true);
    }
}
