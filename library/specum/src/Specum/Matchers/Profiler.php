<?php

namespace Specum\Matchers;

interface Profiler
{
    /**
     * To be true
     * @return void
     */
    public function toBeTrue();

    /**
     * To be false
     * @return void
     */
    public function toBeFalse();

    /**
     * To be truthy
     * @return void 
     */
    public function toBeTruthy();

    /**
     * To be falsey
     * @return void 
     */
    public function toBeFalsey();

    /**
     * To be null
     * @return void 
     */
    public function toBeNull();

    /**
     * To be empty
     * @return void 
     */
    public function toBeEmpty();

    /**
     * To be
     * @param mixed $expected 
     * @return void 
     */
    public function toBe($expected);

    /**
     * To be between
     * @param int|float|double $min 
     * @param int|float|double $max 
     * @return void 
     */
    public function toBeBetween($min, $max);

    /**
     * To match
     * @param string $pattern 
     * @return void 
     */
    public function toMatch($pattern);

    /**
     * @param mixed|null $type 
     * @return void 
     */
    public function toThrow($type = null);

    /**
     * Not to be true
     * @return $this 
     */
    public function notToBeTrue();

    /**
     * Not to be false
     * @return $this 
     */
    public function notToBeFalse();

    /**
     * Not to be truthy
     * @return $this 
     */
    public function notToBeTruthy();

    /**
     * Not to be falsey
     * @return $this 
     */
    public function notToBeFalsey();

    /**
     * Not to be null
     * @return $this 
     */
    public function notToBeNull();

    /**
     * Not to be empty
     * @return $this 
     */
    public function notToBeEmpty();

    /**
     * Not to be
     * @param mixed $expected 
     * @return $this 
     */
    public function notToBe($expected);

    /**
     * Not to be between
     * @param int|float|double $min 
     * @param int|float|double $max 
     * @return $this 
     */
    public function notToBeBetween($min, $max);

    /**
     * not to match
     * @param string $pattern 
     * @return $this 
     */
    public function notToMatch($pattern);

    /**
     * @param mixed|null $type 
     * @return $this 
     */
    public function notToThrow($type = null);
}
