<?php

namespace Loader\Exception;

class FileNotFoundException extends \Exception
{
    public function __construct($fileName)
    {
        parent::__construct("File " + $fileName + " was not found.");
    }
}
