<?php

namespace Loader;

use Loader\Exception\ClassLoaderException;

class ClassLoader
{
    /** 
     * @var array
     */
    private $_prefixes = array();

    /**
     * @var array
     */
    private $_map = array();

    /**
     * @param string $prefix 
     * @return void 
     */
    public function addPrefix($prefix)
    {
        if ($realpath = realpath($prefix)) {
            $this->_prefixes[] = $realpath;
        }
    }

    /**
     * @param string $namespace 
     * @param string $path 
     * @return void 
     */
    public function addMarker($namespace, $path)
    {
        $this->_map[$namespace] = $path;
    }

    /**
     * @param string $namespace 
     * @return void 
     * @throws ClassLoaderException 
     */
    private function autoload($namespace)
    {
        if (isset($this->_map[$namespace])) {
            $path = $this->_map[$namespace];
            if (is_dir($path)) {
                $class_file = $path . DIRECTORY_SEPARATOR . $namespace . '.php';
                if ($realpath = realpath($class_file)) {
                    $this->_require($realpath);
                }
            } elseif (is_file($path)) {
                $this->_require($path);
            }
        } else {
            foreach ($this->_prefixes as $path) {
                if (is_dir($path)) {
                    $class_file = $path . DIRECTORY_SEPARATOR . $namespace . '.php';
                    if ($realpath = realpath($class_file)) {
                        return $this->_require($realpath);
                    }
                }
            }
        }
    }

    /**
     * @param string $class_file 
     * @return void 
     * @throws ClassLoaderException 
     */
    private function _require($class_file)
    {
        if (is_file($class_file)) {
            require $class_file;
        } else
            throw new ClassLoaderException("The file '{$class_file}' cannot be found or does not exist");
    }

    /**
     * @return void 
     */
    public function register()
    {
        spl_autoload_register(array($this, 'autoload'));
    }
}
