<?php

namespace Loader;

use Loader\Exception\ClassMapException;
use Loader\Exception\FileNotFoundException;

class ClassMap
{
    private const CLASS_NAMESPACE_REGEX = '/[ ]*\bnamespace\b\s*([\\\\\w]+)\s*;/';
    private const CLASS_NAME_REGEX = '/[ ]*\b(class|interface|trait)\b\s*([\w]+)\n*/';

    public static function createMap($path, &$array = null)
    {
        if (is_dir($path)) {
            return self::_dirScanner($path, $array);
        } else {
            return self::_fileScanner(realpath($path), $array);
        }
        return array();
    }

    /**
     * @param string $fileName 
     * @return array 
     * @throws FileNotFoundException 
     */
    public static function loadMap($fileName)
    {
        if (file_exists($fileName)) {
            return self::_mapping_decode($fileName);
        } else {
            throw new FileNotFoundException($fileName);
        }
    }

    /**
     * @param string $fileName 
     * @return ClassMapLoader 
     * @throws FileNotFoundException 
     */
    public static function getLoader($fileName)
    {
        return new ClassMapLoader(self::loadMap('./class.mapping.json')['mappings']);
    }

    /**
     * @param string $fileName 
     * @return array 
     * @throws ClassMapException 
     */
    private static function _mapping_decode($fileName)
    {
        $content = file_get_contents($fileName);
        $result = json_decode($content, true);
        if ($result  && json_last_error_msg() !== JSON_ERROR_NONE) {
            return $result;
        } else {
            throw new \ErrorException("Mapped data decoding error.");
        }
    }

    private static function _fileScanner($fileName, &$results = array())
    {
        if (self::_isPhpFile($fileName)) {
            $content = file_get_contents($fileName);
            $className = self::getClassName($content);
            $namespace = self::getClassNamespace($content);
            if ($className && $namespace) {
                $classNamespace = $namespace . '\\' . $className;
                // $results['names'][] = $className;
                // $results['namespaces'][] = $classNamespace;
                $results['mappings'][$classNamespace] = $fileName;
                // $results['sources'][$classNamespace] = $dir;
            }
        }
    }

    /**
     * @param string $dir 
     * @param array|null $results 
     * @return array 
     */
    private static function _dirScanner($dir, &$results = array())
    {
        $content = array_diff(scandir($dir), array('.', '..'));

        foreach ($content as $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (is_dir($path)) {
                self::_dirScanner($path, $results);
            } else {
                self::_fileScanner($path, $results);
            }
        }

        return $results;
    }

    private static function getClassNamespace($fileContent)
    {
        if (preg_match(self::CLASS_NAMESPACE_REGEX, $fileContent, $matches)) {
            return $matches[1];
        }
        return false;
    }

    private static function getClassName($fileContent)
    {
        if (preg_match(self::CLASS_NAME_REGEX, $fileContent, $matches)) {
            return $matches[2];
        }
        return false;
    }

    /**
     * @param string $file 
     * @return bool 
     */
    private static function _isPhpFile($file)
    {
        return preg_match('/\w+.php$/i', basename($file)) === 1;
    }
}
