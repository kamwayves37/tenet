<?php

namespace Loader;

use Loader\Exception\ClassLoaderException;

class ClassMapLoader
{
    /**
     * @var array
     */
    private $_map = array();

    /**
     * @param array$map 
     */
    public function __construct($mapping)
    {
        if (is_array($mapping)) {
            $this->_map = $mapping;
        }
    }

    private function autoload($namespace)
    {
        if (isset($this->_map[$namespace])) {
            $this->_require($this->_map[$namespace]);
        }
    }

    /**
     * @param string $class_file 
     * @return void 
     * @throws ClassLoaderException 
     */
    private function _require($class_file)
    {
        if (file_exists($class_file)) {
            require $class_file;
        } else
            throw new ClassLoaderException("The file '{$class_file}' cannot be found or does not exist");
    }

    /**
     * @return void 
     */
    public function register()
    {
        spl_autoload_register(array($this, 'autoload'));
    }
}
