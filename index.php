<?php

use Room\Loader\ClassLoader;
use Room\Annotation\Annotation;
use Room\Annotation\AnnotationDrap as Drap;
use Room\Annotation\AnnotationDrap;
use Room\Annotation\DefaultValue;
use Room\Annotation\Target;
use Room\Reflector\RoomAnnotationParser;

/**
 * @Annotation
 * @Target(
 * {
 * 'METHOD',
 *  'CLASS',
 *  'PROPERTY'
 * }
 * )
 */
interface Test
{
    /**
     * @DefaultValue(10)
     * @Required
     */
    public function x(): int;

    /**
     * @DefaultValue(0)
     * @Required
     */
    public function y(): int;
}

/**
 * @Test(y = 2)
 * @Drap(1)
 */
class AnnotationTest
{
    /**
     * @Test(x=5, y=1)
     * @Drap(1)
     */
    private $name;

    /**
     * @Test(x=5, y=1)
     * @Drap(1)
     */
    private $pseudo;

    /**
     * @Test(x=5, y=1)
     * @Drap(1)
     */
    public function myMethodA()
    {
    }
}


require_once './library/room/src/Room/Loader/ClassLoader.php';

ClassLoader::register();

$reflection = new \ReflectionClass(AnnotationTest::class);

$parser = new RoomAnnotationParser();

$anno = $parser->getAnnotationOfClass($reflection, Test::class);
var_dump($anno);
